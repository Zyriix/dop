const PIPELINE_EN_US = {
    'pipeline.hello': 'Hello, world!',
    'pipeline.name': 'my name is {name}',

    'pipeline.newPipeline': 'Create a new pipeline',
    'pipeline.table.index': 'Index',
    'pipeline.table.name': 'Name',
    'pipeline.table.createTime': 'Create-Time',
    'pipeline.table.creator': 'Creator',
    'pipeline.table.operation': 'Operation',
    'pipeline.table.operation.edit': 'Edit',
    'pipeline.table.operation.check': 'Check',
    'pipeline.table.operation.delete': 'Delete',
    'pipeline.table.operation.language': 'en-us',
    'pipeline.table.operation.deleteSuccess': 'Success',
    'pipeline.table.operation.deleteFailure': 'Failure',

    'pipeline.project.runPipeline': 'Run Pipeline',
    'pipeline.project.editPipeline': 'Edit Pipeline',
    'pipeline.project.deletePipeline': 'Delete Pipeline',
};
export default PIPELINE_EN_US;
