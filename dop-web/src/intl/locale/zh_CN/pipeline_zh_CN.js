const PIPELINE_ZH_CN = {
    'pipeline.hello': '你好，世界！',
    'pipeline.name': '我的名字是 {name}',
    'pipeline.newPipeline': '新建流水线',

    'pipeline.table.index': '序号',
    'pipeline.table.name': '流水线名称',
    'pipeline.table.createTime': '创建时间',
    'pipeline.table.creator': '创建人',
    'pipeline.table.operation': '操作',
    'pipeline.table.operation.edit': '编辑',
    'pipeline.table.operation.delete': '删除',
    'pipeline.table.operation.check': '查看',
    'pipeline.table.operation.language': 'zh-cn',
    'pipeline.table.operation.deleteSuccess': '删除成功',
    'pipeline.table.operation.deleteFailure': '删除失败',

    'pipeline.project.runPipeline': '运行流水线',
    'pipeline.project.editPipeline': '编辑流水线',
    'pipeline.project.deletePipeline': '删除流水线',
};
export default PIPELINE_ZH_CN;
