/**
 *  流水线信息展示，修改
 *  @author zhangfuli
 *
 * */

import React, {Component} from 'react';
import {FormBinderWrapper, FormBinder, FormError} from '@icedesign/form-binder';
import {Input, Button, Select} from '@icedesign/base';
import {Link} from 'react-router-dom';
import Jenkinsfile from '../components/Jenkinsfile'
import PipelineInfoStage from '../components/PipelineInfoStage'
import './PipelineInfo.scss';
import Axios from 'axios';
import API from '../../API';
import {Feedback} from "@icedesign/base/index";

const {Combobox} = Select;
const {toast} = Feedback;

export default class PipelineInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // 流水线的基本信息
            pipeline: {
                name: "",
                cuser: window.sessionStorage.getItem('user-id'),
                //监听设置
                monitor: "",
                appId: null,
                appEnvId: null,
                config: "无Jenkinsfile",
                stages: [],
                jenkinsfile: {}
            },
            monitor: ["自动触发"],
            jenkinsFile: ["自带Jenkinsfile", "无Jenkinsfile"],
            haveJenkinsFile: null,
            jenkinsFileInfo: {
                git: "",
                path: ""
            },
            currentStage: 0,
            applications: []
        };
    }

    componentWillMount() {
        this.getApplication()
    }

    getApplication() {
        let url = API.application + "/app?ouser=" + window.sessionStorage.getItem('user-id');
        let self = this;
        let applications = self.state.applications;
        Axios.get(url).then((response) => {
            if (response.status === 200) {
                for (let i = 0; i < response.data.length; i++) {
                    let application = {
                        label: response.data[i].title,
                        value: response.data[i].id
                    };
                    applications.push(application)
                }
                self.setState({
                    applications: applications
                })
            }
        })
    }

    /**
     *  基本信息
     * */
    formChange = value => {
        this.setState({value});
    };

    /**
     * 选择监听方式更新数据
     * */
    selectMonitor(value) {
        let pipeline = Object.assign({}, this.state.pipeline, {monitor: value});
        this.setState({
            pipeline
        });
    }

    selectApplication(value) {
        let pipeline = this.state.pipeline;
        pipeline.appId = value;
        this.setState({
            pipeline
        });
    }


    selectJenkinsFile(value) {
        this.setState({
            haveJenkinsFile: value
        })
    }

    jenkinsFileData(jenkinsFileInfo) {
        this.setState({
            jenkinsFileInfo
        });
    }

    setStages(value, currentStage) {
        let pipeline = Object.assign({}, this.state.pipeline, {stages: value});
        this.setState({
            pipeline: pipeline,
            currentStage: currentStage
        })
    }
    onSelectEnv(value){
        let pipeline = this.state.pipeline;
        pipeline.appEnvId = value;
        this.setState({
            pipeline
        })
    }
    saveJenkinsfile() {
        let pipeline = this.state.pipeline;
        let jenkinsFileInfo = this.state.jenkinsFileInfo;
        let self = this;
        pipeline.jenkinsfile = jenkinsFileInfo;
        let url = API.pipeline + "/v1/pipeline/jenkinsfile";
        Axios.post(url, pipeline).then((response) => {
            if (response.status === 200) {
                toast.show({
                    type: "success",
                    content: "保存成功",
                    duration: 1000
                });
                self.props.history.push('/pipeline')
            }
        }).catch(() => {
            toast.show({
                type: "error",
                content: "请检查您的路径",
                duration: 1000
            });
        })
    }

    save() {
        let self = this;
        let url = API.pipeline + '/v1/pipeline';
        toast.show({
            type: "loading",
            content: "正在提交请稍后",
            duration: 4000
        });
        Axios({
            method: 'post',
            url: url,
            data: self.state.pipeline
        }).then((response) => {
            if (response.status === 200) {
                toast.show({
                    type: "success",
                    content: "保存成功",
                    duration: 1000
                });
                self.props.history.push('/pipeline')
            }
        }).catch((error)=>{
            toast.show({
                type: "error",
                content: "保存失败",
                duration: 1000
            });
        })
    }


    render() {
        return (
            <div className="pipeline-info-body">
                <FormBinderWrapper
                    value={this.state.pipeline}
                    onChange={this.formChange}
                    ref="form"
                >
                    <div className="form-body">
                        <div className="form-item">
                            <span className="form-item-label">流水线名称: </span>
                            <FormBinder name="name" required message="请输入流水线的名称">
                                <Input placeholder="请输入流水线的名称" className="combobox"/>
                            </FormBinder>
                            <FormError className="form-item-error" name="name"/>
                        </div>
                        <div className="form-item">
                            <span className="form-item-label">监听设置: </span>
                            <FormBinder name="monitor" required message="请选择监听设置">
                                <Combobox
                                    onChange={this.selectMonitor.bind(this)}
                                    dataSource={this.state.monitor}
                                    placeholder="请选择监听设置"
                                    className="combobox"
                                >
                                </Combobox>
                            </FormBinder>
                            <FormError className="form-item-error" name="monitor"/>
                        </div>

                        <div className="form-item">
                            <span className="form-item-label">应用设置: </span>
                            <FormBinder name="appId" required message="请选择应用">
                                <Combobox
                                    onChange={this.selectApplication.bind(this)}
                                    dataSource={this.state.applications}
                                    placeholder="请选择应用"
                                    fillProps="label"
                                    className="combobox"
                                >
                                </Combobox>
                            </FormBinder>
                            <FormError className="form-item-error" name="appId"/>
                        </div>

                        <div className="form-item">
                            <span className="form-item-label">配置流水线: </span>
                            <FormBinder name="config" required message="配置设置">
                                <Combobox
                                    onChange={this.selectJenkinsFile.bind(this)}
                                    dataSource={this.state.jenkinsFile}
                                    placeholder="配置设置"
                                    className="combobox"
                                >
                                </Combobox>
                            </FormBinder>
                            <FormError className="form-item-error" name="config"/>
                        </div>

                        {(() => {
                            if (this.state.haveJenkinsFile === '自带Jenkinsfile') {
                                return (
                                    <Jenkinsfile
                                        jenkinsfile={this.state.jenkinsFileInfo}
                                        onChange={this.jenkinsFileData.bind(this)}
                                    />
                                )
                            } else {
                                return (
                                    <PipelineInfoStage
                                        stages={this.state.pipeline.stages}
                                        currentStage={this.state.currentStage}
                                        appId={this.state.pipeline.appId}
                                        onChange={this.setStages.bind(this)}
                                        onSelectEnv = {this.onSelectEnv.bind(this)}
                                    />
                                )
                            }
                        })()}


                    </div>
                </FormBinderWrapper>


                <div className="footer">
                    <div className="footer-container">
                        <Button type="primary"
                                className="button"
                                onClick={
                                    this.state.haveJenkinsFile === "自带Jenkinsfile" ?
                                        this.saveJenkinsfile.bind(this) : this.save.bind(this)
                                }
                        >保存</Button>
                        <Link to='/pipeline'>
                            <Button type="normal"
                                    className="button"
                            >取消</Button>
                        </Link>

                    </div>
                </div>
            </div>
        );
    }
}
