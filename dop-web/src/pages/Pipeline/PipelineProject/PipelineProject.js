/**
 *  流水线信息展示，修改
 *  @author zhangfuli
 *
 * */
import React, {Component} from 'react';
import {Button, Icon, Loading, Feedback} from '@icedesign/base';
import Axios from 'axios';
import API from '../../API';
import RunResult from './RunResult'
import {NotPermission} from '../../NotFound'
import {FormattedMessage} from 'react-intl';
import './PipelineProject.scss'

const {toast} = Feedback;

export default class PipelineProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            pipelineId: this.props.match.params.id,
            pipeline: {},
            runs: {
                _links: {
                    self: {
                        href: ""
                    }
                }
            },
            queue: [],
            time: "",
            resultStatus: "RUN",
            notRunning: true
        }
    }

    componentDidMount() {
        let self = this;
        self.setState({
            visible: true
        });
        self.getPipelineInfo();
        let time = setInterval(() => {
            this.getRuns().then((data) => {
                self.setState({
                    runs: data,
                    visible: false
                })
            })
        }, 5000);
        self.setState({
            time: time
        });
    }

    getRuns() {
        let url = API.pipeline + '/v1/jenkins/runs?id=' + this.state.pipelineId;
        let self = this;
        return new Promise((resolve, reject) => {
            Axios.get(url).then((response) => {
                if (response.status === 200) {
                    if (response.data.length === 0) {
                        toast.show({
                            type: "prompt",
                            content: "该流水线尚未运行",
                            duration: 3000
                        });
                        self.setState({
                            notRunning: true
                        });
                        resolve(response.data[0]);
                        self.clear();
                    } else {
                        self.setState({
                            notRunning: false
                        });
                        resolve(response.data[0]);
                        if (response.data[0].state === 'FINISHED') {
                            self.clear();
                            if (self.state.resultStatus === "BUILD") {
                                self.setResult();
                            }
                        }
                    }
                }
                reject()
            }).catch((error) => {
                let self = this;
                if (error.response.status === 500 && error.response.data.message === "404 Not Found") {
                    toast.show({
                        type: "prompt",
                        content: "该流水线尚未运行",
                        duration: 3000
                    });
                }
                self.setState({
                    visible: false
                });
                self.clear();
            })
        })
    }

    getPipelineInfo() {
        let url = API.pipeline + '/v1/pipeline/' + this.state.pipelineId;
        let self = this;

        Axios.get(url).then((response) => {
            if (response.status === 200) {
                self.setState({
                    pipeline: response.data
                })
            }
        })
    }

    buildPipeline() {
        let self = this;
        self.setState({
            visible: true,
            resultStatus: "BUILD",
            notRunning: false
        });
        let url = API.pipeline + '/v1/jenkins/build?id=' + this.state.pipelineId;
        Axios.post(url).then((response) => {
            if (response.status === 200) {
                let time = setInterval(() => {
                    this.getRuns().then((data) => {
                        self.setState({
                            runs: data,
                            visible: false
                        })
                    })
                }, 5000);
                self.setState({
                    runs: response.data,
                    time: time
                });
            }
        }).catch((error)=>{
            toast.show({
                type: "error",
                content: "启动运行失败",
                duration: 3000
            });
            self.setState({
                visible: false,
                resultStatus: "RUN"
            });
        })
    }

    clear() {
        clearInterval(this.state.time);
    }

    setResult() {
        this.setState({
            resultStatus: "RUN"
        });
        let url = API.pipeline + '/v1/resultOutput/notify/' + this.state.pipelineId;
        Axios.post(url).then((response) => {
        }).catch(() => {
            toast.show({
                type: "error",
                content: "此次执行信息丢失",
                duration: 3000
            });
        })
    }

    removeByIdSQL(id) {
        let url = API.pipeline + '/v1/delete/' + id;
        let self = this;
        Axios.put(url).then((response) => {
            if (response.status === 200) {
                toast.show({
                    type: "success",
                    content: "删除成功",
                    duration: 1000
                });
                self.props.history.push('/pipeline')
            }
        })
    }

    editPipeline() {
        this.props.history.push("/pipeline/edit/" + this.state.pipelineId)
    }

    deletePipeline() {
        let url = API.pipeline + '/v1/jenkins/' + this.state.pipelineId;
        let self = this;
        self.setState({
            visible: true
        });
        Axios({
            method: 'delete',
            url: url,
        }).then((response) => {
            if (response.status === 200) {
                self.removeByIdSQL(this.state.pipelineId);
            } else {
                toast.show({
                    type: "error",
                    content: "删除失败",
                    duration: 1000
                });
            }
        });
    }

    render() {
        return (
            <div className="body">
                <Loading shape="fusion-reactor" visible={this.state.visible} className="next-loading my-loading">
                    <div className="operate">
                        <Button type="primary" className="button" onClick={this.buildPipeline.bind(this)}>
                            <Icon type="play"/>
                            <FormattedMessage
                                id="pipeline.project.runPipeline"
                                defaultMessage="运行流水线"
                            />
                        </Button>
                        <Button type="normal" className="button" onClick={this.editPipeline.bind(this)}>
                            <Icon type="edit"/>
                            <FormattedMessage
                                id="pipeline.project.editPipeline"
                                defaultMessage=" 编辑流水线"
                            />
                        </Button>
                        <Button type="secondary" shape="warning" className="button"
                                onClick={this.deletePipeline.bind(this)}>
                            <Icon type="ashbin"/>
                            <FormattedMessage
                                id="pipeline.project.deletePipeline"
                                defaultMessage="删除流水线"
                            />
                        </Button>
                    </div>

                    {(()=>{
                        if(this.state.notRunning){
                            return <NotPermission />
                        }else{
                            return  <RunResult runs={this.state.runs}/>
                        }
                    })()}
                </Loading>
            </div>

        );
    }
}
