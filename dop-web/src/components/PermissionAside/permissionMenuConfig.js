const permissionAsideMenuConfig=[
    {
        name: '首页',
        path: '/',
        icon: 'home2'
    },
    {
        name: '功能点管理',
        path: '/permission/permissions',
        icon: 'box' ,
    },
    {
        name: '角色管理',
        path: '/permission/roles',
        icon: 'account',
    },

    {
        name: '用户管理',
        path: '/permission/roles/userwithrole',
        icon: 'account-filling',
    },
    {
        name: '数据权限',
        path: '/permission/dataRules',
        icon: 'filter',
    },

]
export {permissionAsideMenuConfig};