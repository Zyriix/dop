package com.clsaa.dop.server.test.enums;

public enum HttpMethod {

    POST,
    GET,
    PUT,
    PATCH,
    DELETE,
    HEAD,
    OPTIONS;

    private HttpMethod() {
    }
}